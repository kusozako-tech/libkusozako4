
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version('Adw', '1')
gi.require_version('Gtk', '4.0')
