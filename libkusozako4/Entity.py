
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


class DeltaEntity:

    def _decode_message(self, message, header):
        message = message.replace("delta > ", header)
        return message.replace(" ", "_")

    def _on_method_found(self, message, user_data=None):
        method = getattr(self, message)
        return method() if user_data is None else method(user_data)

    def _enquiry(self, message, user_data=None):
        message = self._decode_message(message, "_delta_info_")
        return self._request_synchronization(message, user_data)

    def _raise(self, message, user_data=None):
        message = self._decode_message(message, "_delta_call_")
        return self._request_synchronization(message, user_data)

    def _request_synchronization(self, message, user_data=None):
        if self._parent is not None:
            return self._parent.synchronize_delta_entity(message, user_data)

    def synchronize_delta_entity(self, message, user_data=None):
        if message in dir(self):
            return self._on_method_found(message, user_data)
        else:
            return self._request_synchronization(message, user_data)
