# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

UNIT = 8
HEADERBAR_BORDER = UNIT
BOLD_HEADERBAR_HEIGHT = UNIT*6
