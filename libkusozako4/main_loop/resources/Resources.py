# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
from libkusozako4.Entity import DeltaEntity


class DeltaResources(DeltaEntity):

    def _set_css(self):
        path = GLib.build_filenamev([GLib.get_home_dir(), "test.css"])
        _, bytes_ = GLib.file_get_contents(path)
        css_provider = Gtk.CssProvider()
        # css_provider.load_from_data(bytes(bytes_, "utf-8"))
        css_provider.load_from_data(bytes_)
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )

    def __init__(self, parent):
        self._parent = parent
        self._set_css()
        self._raise("delta > loopback resources ready", self)
