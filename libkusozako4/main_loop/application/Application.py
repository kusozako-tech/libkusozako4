# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
from gi.repository import Adw
from gi.repository import Gio
from libkusozako4.Entity import DeltaEntity


class DeltaApplication(Adw.Application, DeltaEntity):

    def do_activate(self):
        self._raise("delta > loopback application ready", self)

    def _delta_info_adw_application(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        Adw.Application.__init__(
            self,
            application_id="com.gitlab.kusozako-tech.gen4",
            flags=Gio.ApplicationFlags.FLAGS_NONE,
            )
        self.run(sys.argv)
