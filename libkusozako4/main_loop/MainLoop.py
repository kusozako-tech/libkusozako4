# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako4.Entity import DeltaEntity
from .resources.Resources import DeltaResources
from .application.Application import DeltaApplication
from .main_window.MainWindow import DeltaMainWindow


class AlfaMainLoop(DeltaEntity):

    def _delta_call_loopback_application_ready(self, parent):
        DeltaMainWindow(parent)

    def _delta_call_loopback_resources_ready(self, parent):
        DeltaApplication(parent)

    def _request_synchronization(self, message, user_data=None):
        print("delta > message uncaught", message, user_data)

    def __init__(self):
        parent = None
        DeltaResources(self)
