
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import Gio
from libkusozako4.Entity import DeltaEntity
from .RootContainer import DeltaRootContainer


class DeltaMainWindow(Adw.ApplicationWindow, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.set_content(widget)

    def __init__(self, parent):
        self._parent = parent
        Adw.ApplicationWindow.__init__(
            self,
            application=self._enquiry("delta > adw application")
            )
        self.style_manager = Adw.StyleManager().get_default()
        self.set_size_request(400, 400)
        self.get_style_context().add_class("main-window")
        DeltaRootContainer(self)
        self.present()
