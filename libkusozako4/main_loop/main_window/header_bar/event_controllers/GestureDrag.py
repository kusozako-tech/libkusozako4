
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository.Gdk import SurfaceEdge
from libkusozako4.Entity import DeltaEntity

EDGES = [
    [SurfaceEdge.NORTH_WEST, SurfaceEdge.NORTH, SurfaceEdge.NORTH_EAST],
    [SurfaceEdge.WEST, None, SurfaceEdge.EAST],
    [SurfaceEdge.WEST, None, SurfaceEdge.EAST]
    ]

BORDER_WIDTH = 8

class DeltaGestureDrag(Gtk.GestureDrag, DeltaEntity):

    def _get_index(self, size, position):
        if BORDER_WIDTH > position:
            return 0
        elif size-BORDER_WIDTH > position > BORDER_WIDTH:
            return 1
        return 2

    def _get_edge_type(self, widget, offset_x, offset_y):
        x_index = self._get_index(widget.get_allocated_width(), offset_x)
        y_index = self._get_index(widget.get_allocated_height(), offset_y)
        return EDGES[y_index][x_index]

    def _on_drag_begin(self, gesture, offset_x, offset_y):
        widget = gesture.get_widget()
        edge_type = self._get_edge_type(widget, offset_x, offset_y)
        surface = widget.get_root().get_surface()
        device = gesture.get_current_event_device()
        args = device, 1, offset_x, offset_y, Gdk.CURRENT_TIME
        if edge_type is None:
            surface.begin_move(*args)
        else:
            surface.begin_resize(edge_type, *args)

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
        Gtk.GestureDrag.__init__(self, button=1)
        self.connect("drag-begin", self._on_drag_begin)
        self._raise("delta > add controller", self)
