# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .GestureClick import DeltaGestureClick
from .GestureDrag import DeltaGestureDrag
from .EventControllerMotion import DeltaEventControllerMotion


class EchoEventControllers:

    def __init__(self, parent):
        DeltaGestureDrag(parent)
        DeltaGestureClick(parent)
        DeltaEventControllerMotion(parent)
