
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository.Gdk import SurfaceEdge
from libkusozako4 import Size
from libkusozako4.Entity import DeltaEntity

EDGES = [
    ["nw-resize", "n-resize", "ne-resize"],
    ["w-resize", "default", "e-resize"],
    ["w-resize", "default", "e-resize"]
    ]
BORDER_WIDTH = Size.HEADERBAR_BORDER


class DeltaEventControllerMotion(Gtk.EventControllerMotion, DeltaEntity):

    def _get_index(self, size, position):
        if BORDER_WIDTH > position:
            return 0
        elif size-BORDER_WIDTH > position > BORDER_WIDTH:
            return 1
        return 2

    def _get_edge_type(self, widget, offset_x, offset_y):
        x_index = self._get_index(widget.get_allocated_width(), offset_x)
        y_index = self._get_index(widget.get_allocated_height(), offset_y)
        return EDGES[y_index][x_index]

    def _on_motion(self, event_controller, x, y):
        widget = event_controller.get_widget()
        edge_type = self._get_edge_type(widget, x, y)
        cursor = Gdk.Cursor.new_from_name(edge_type)
        surface = widget.get_root().get_surface()
        surface.set_cursor(cursor)

    def _on_leave(self, event_controller):
        widget = event_controller.get_widget()
        cursor = Gdk.Cursor.new_from_name("default")
        surface = widget.get_root().get_surface()
        surface.set_cursor(cursor)

    def __init__(self, parent):
        self._parent = parent
        Gtk.EventControllerMotion.__init__(self)
        self.connect("motion", self._on_motion)
        self.connect("leave", self._on_leave)
        self._raise("delta > add controller", self)
