# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository.Gdk import SurfaceEdge
from libkusozako4.Entity import DeltaEntity


class DeltaGestureClick(Gtk.GestureClick, DeltaEntity):

    def _toggle_maximized(self, root_window):
        if root_window.is_maximized():
            root_window.unmaximize()
        else:
            root_window.maximize()

    def _on_pressed(self, gesture, n_press, x, y):
        if n_press == 2:
            root_window = gesture.get_widget().get_root()
            self._toggle_maximized(root_window)

    def __init__(self, parent):
        self._parent = parent
        Gtk.GestureClick.__init__(self, button=1, exclusive=True)
        self.connect("released", self._on_pressed)
        self._raise("delta > add controller", self)
