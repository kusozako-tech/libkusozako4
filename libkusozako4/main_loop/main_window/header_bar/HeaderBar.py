# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako4.Entity import DeltaEntity
from .event_controllers.EventControllers import EchoEventControllers
from .styles.Styles import EchoStyles


class DeltaHeaderBar(Gtk.Stack, DeltaEntity):

    def _delta_call_add_controller(self, controller):
        return self.add_controller(controller)

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def __init__(self, parent):
        settings = Gtk.Settings()
        self._parent = parent
        Gtk.Stack.__init__(self)
        self.get_style_context().add_class("kusozako-headerbar")
        EchoEventControllers(self)
        EchoStyles(self)
        self._raise("delta > add to container", self)
