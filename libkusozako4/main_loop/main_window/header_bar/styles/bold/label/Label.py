# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako4 import Size
from libkusozako4.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(self, label="foobar")
        self.set_size_request(-1, Size.BOLD_HEADERBAR_HEIGHT)
        self._raise("delta > set center widget", self)
