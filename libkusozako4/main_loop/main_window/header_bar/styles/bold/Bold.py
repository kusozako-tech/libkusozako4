# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako4 import Size
from libkusozako4.Entity import DeltaEntity
from libkusozako4 import HeaderbarStyles
from .label.Label import DeltaLabel
from .MenuButton import DeltaMenuButton


class DeltaBold(Gtk.CenterBox, DeltaEntity):

    def _delta_call_set_center_widget(self, widget):
        self.set_center_widget(widget)

    def _delta_call_set_end_widget(self, widget):
        self.set_end_widget(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaLabel(self)
        DeltaMenuButton(self)
        user_data = self, HeaderbarStyles.BOLD
        self._raise("delta > add to stack named", user_data)
