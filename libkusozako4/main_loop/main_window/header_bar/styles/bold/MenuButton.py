# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako4 import Size
from libkusozako4.Entity import DeltaEntity


class DeltaMenuButton(Gtk.MenuButton, DeltaEntity):

    def _on_pressed(self, gesture, utton, x, y, popover):
        popover.popup()

    def __init__(self, parent):
        self._parent = parent
        Gtk.MenuButton.__init__(
            self,
            margin_top=8,
            margin_bottom=8,
            margin_end=8,
            )
        self.set_icon_name("open-menu-symbolic")
        self.get_style_context().add_class("kusozako-popover-button")
        popover = Gtk.Popover()
        popover.get_style_context().add_class("kusozako-popover")
        button = Gtk.Button(label="foobar")
        button.get_style_context().add_class("kusozako-popover-button")
        popover.set_child(button)
        self.set_popover(popover)
        gesture = Gtk.GestureClick(button=1)
        gesture.connect("pressed", self._on_pressed, popover)
        self.add_controller(gesture)
        self._raise("delta > set end widget", self)
